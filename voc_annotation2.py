import xml.etree.ElementTree as ET
import os
from os import getcwd

train_sets=['01','02','03','04','05','06']
val_sets=[ '07','08','09','10']

classes = ["person"]

img_path = getcwd() + '/data/images'
anno_path = getcwd() + '/data/annotations'

def convert(size, box):
    dw = 1./(size[0])
    dh = 1./(size[1])
    x = (box[0] + box[1])/2.0 - 1
    y = (box[2] + box[3])/2.0 - 1
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)


def convert_annotation(jpg_label, anno_file):

   tree=ET.parse(anno_file)
   root = tree.getroot()
   size = root.find('size')
   w = int(size.find('width').text)
   h = int(size.find('height').text)

   for obj in root.iter('object'):
        difficult = obj.find('difficult').text
        cls = obj.find('name').text
        if cls not in classes or int(difficult)==1:
           continue
        cls_id = classes.index(cls)
        xmlbox = obj.find('bndbox')
        b = (int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)), int(float(xmlbox.find('xmax').text)), int(float(xmlbox.find('ymax').text)))
        bb = convert((w,h), b)
        jpg_label.write(str(cls_id)+ " " + " ".join([str(a) for a in bb]) + '\n')
def main():

   train_file = open('train.txt', 'w')
   val_file = open('validation.txt', 'w')
   anno_dir = './annotations'

   for set in train_sets:
      set_id = str(set).zfill(2)
      set_path = '/set{}'.format(set_id)
      for dir in range(0, len(os.listdir(img_path + set_path))):
         vid_id = '{}'.format(str(dir).zfill(3))
         vid_path = '/V{}/images'.format(vid_id)
         for jpg in os.listdir(img_path + set_path + vid_path):
            jpg_id = jpg.strip('I').lstrip('0').strip('.jpg')
            jpg_label = open('{}.txt'.format(jpg.strip('.jpg')), 'w')
            try:
              os.chdir(img_path + set_path + vid_path)
              anno_file = open(anno_path + '/bbox/set%s_V%s_%s.xml'%(set_id, vid_id, jpg_id))
              convert_annotation(jpg_label, anno_file)
              jpg_label.close()
            except IOError:
               print('File' +  set_id+'_'+'V'+vid_id+'_'+jpg_id+' doesnt exist')
               continue
   print('Finished train sets')

   train_file.close()

   for set in val_sets:
      set_id = str(set).zfill(2)
      set_path = '/set{}'.format(set_id)
      for dir in range(0, len(os.listdir(img_path + set_path))):
         vid_id = '{}'.format(str(dir).zfill(3))
         vid_path = '/V{}/images'.format(vid_id)
         for jpg in os.listdir(img_path + set_path + vid_path):
            jpg_id = jpg.strip('I').lstrip('0').strip('.jpg')
            jpg_label = open('{}.txt'.format(jpg.strip('.jpg')), 'w')
            try:
               os.chdir(img_path + set_path + vid_path)
               anno_file = open(anno_path + '/bbox/set%s_V%s_%s.xml'%(set_id, vid_id, jpg_id))
               convert_annotation(jpg_label, anno_file)
               jpg_label.close()
            except IOError:
               print('File' +  set_id+'_'+vid_id+'_'+jpg_id+' doesnt exist')
               continue
   print('Finished validation sets')

   val_file.close()

if __name__ == '__main__':
   main()

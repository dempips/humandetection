from flask import Flask, render_template, Response
import sys
import argparse
from yolo import YOLO, detect_video
from PIL import Image
from camera import Camera
import cv2

app = Flask(__name__)
FLAGS = None
trained_model = 'model_data/darkyolo.h5'

@app.route('/')
def index():
 #   camera  = cv2.VideoCapture('rtsp://1.144.111.236:8080')
#    if(camera):
#       print('Successfully connected to camera')
#    return Response(gen(Camera()),
#                    mimetype='multipart/x-mixed-replace; boundary=frame')
#    else:
#       print('Failed connecting to camera')

    return render_template('index.html')
def get_frame(camera):
        success, image = camera.read()
        if(success):
            print ('success')
        else:
             print('failed')

        # We are using Motion JPEG, but OpenCV defaults to capture raw images,
        # so we must encode it into JPEG in order to correctly display the
        # video stream.
        ret, jpeg = cv2.imencode('.jpg', image)[1].tobytes()
        #frame = Image.fromarray()
        return jpeg

def gen(camera):
    while True:
        #frame = detect_video(trained_model, camera)
        frame  = get_frame(camera)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame  + b'\r\n\r\n')

@app.route('/video_feed')
def video_feed():
#    return 'video feed'
     return Response(gen(cv2.VideoCapture('http//172.19.115.248:8090/video?dummy=param.mjpg')),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)

import xml.etree.ElementTree as ET
import os
from os import getcwd

train_sets=['01','02','03','04','05','06', '07']
val_sets=['08','09','10']

classes = ["person"]

img_path = getcwd() + '/data/images'
anno_path = getcwd() + '/data/annotations'


def convert_annotation(set_id, vid_id, jpg_id, list_file, anno_file):

   tree=ET.parse(anno_file)
   root = tree.getroot()

   for obj in root.iter('object'):
        difficult = obj.find('difficult').text
        cls = obj.find('name').text
        if cls not in classes or int(difficult)==1:
           continue
        cls_id = classes.index(cls)
        xmlbox = obj.find('bndbox')
        b = (int(float(xmlbox.find('xmin').text)), int(float(xmlbox.find('ymin').text)), int(float(xmlbox.find('xmax').text)), int(float(xmlbox.find('ymax').text)))
        list_file.write(" " + ",".join([str(a) for a in b]) + ',' + str(cls_id))
   return list_file

def main():

   train_file = open('train.txt', 'w')
   val_file = open('validation.txt', 'w')

   for set in train_sets:
      set_id = str(set).zfill(2)
      set_path = '/set{}'.format(set_id)
      for dir in range(0, len(os.listdir(img_path + set_path))):
         vid_id = '{}'.format(str(dir).zfill(3))
         vid_path = '/V{}/images'.format(vid_id)
         for jpg in os.listdir(img_path + set_path + vid_path):
            jpg_id = jpg.strip('I').lstrip('0').strip('.jpg')
            try:
              anno_file = open(anno_path + '/bbox/set%s_V%s_%s.xml'%(set_id, vid_id, jpg_id))
              train_file.write(img_path + set_path + vid_path + '/{}'.format(jpg))
            #  train_file = convert_annotation(set_id , vid_id, jpg_id, train_file, anno_file)
              train_file.write('\n')
            except IOError:
               print('File' +  set_id+'_'+'V'+vid_id+'_'+jpg_id+' doesnt exist')
               continue
   print('Finished train.txt')

   train_file.close()

   for set in val_sets:
      set_id = str(set).zfill(2)
      set_path = '/set{}'.format(set_id)
      for dir in range(0, len(os.listdir(img_path + set_path))):
         vid_id = '{}'.format(str(dir).zfill(3))
         vid_path = '/V{}/images'.format(vid_id)
         for jpg in os.listdir(img_path + set_path + vid_path):
            jpg_id = jpg.strip('I').lstrip('0').strip('.jpg')
            try:
               anno_file = open(anno_path + '/bbox/set%s_V%s_%s.xml'%(set_id, vid_id, jpg_id))
               val_file.write(img_path + set_path + vid_path + '/{}'.format(jpg))
               val_file = convert_annotation(set_id , vid_id, jpg_id, val_file, anno_file)
               val_file.write('\n')
            except IOError:
               print('File' +  set_id+'_'+vid_id+'_'+jpg_id+' doesnt exist')
               continue
   print('Finished validation.txt')

   val_file.close()

if __name__ == '__main__':
   main()
